﻿from comet_ml import Experiment
import numpy as np
import dlipr
import speckles
from tensorflow import keras
import matplotlib.pyplot as plt
models = keras.models
layers = keras.layers
backend = keras.backend


################################################################################
#
# Connecting to Comet
#
################################################################################


experiment = Experiment(api_key="E7YQLfWBuQZXjw0c2dKsXKPsY",
                        project_name="exercise8", workspace="irratzo")



################################################################################
#
# Preprocessing data
#
################################################################################



def preprocess_data(data, norm_data):
    # logarithmic intensity values
    data = np.log10(data + 0.01)
    norm_data = np.log10(norm_data + 0.01)
    # norm input data to max. value of undistorted scattering pattern
    max_val = np.max(norm_data, axis=1)
    data = data / max_val.reshape(len(max_val), 1)
    # reshape data for convolutional network
    data = np.reshape(data, (len(data), 64, 64, 1))
    # limit maximum intensity values
    data = np.clip(data, 0., 1.1)

    return data


(x_train_noisy, x_train), (x_test_noisy, x_test) = speckles.load_data()

x_train_noisy = preprocess_data(x_train_noisy, x_train)
x_train = preprocess_data(x_train, x_train)
x_test_noisy = preprocess_data(x_test_noisy, x_test)
x_test = preprocess_data(x_test, x_test)

speckles.plot_examples(x_test_noisy, x_test, fname='speckle_examples.png')
print(x_train_noisy.shape)




################################################################################
#
# Defining network
#
################################################################################




z1=keras.layers.Input(shape=(64,64,1))

z2 = keras.layers.Conv2D(256,(3,3), activation="relu",padding="same")(z1)
z3 = keras.layers.Conv2D(128,(3,3), activation="relu",padding="same")(z2)
z4 = keras.layers.MaxPooling2D((2,2),padding="same")(z3)
z5 = keras.layers.Conv2D(64,(3,3), activation="relu",padding="same")(z4)
z6 = keras.layers.Conv2D(32,(3,3), activation="relu",padding="same")(z5)
z7 = keras.layers.MaxPooling2D((2,2),padding="same")(z6)
z8 = keras.layers.Conv2D(16,(3,3), activation="relu",padding="same")(z7)
z9 = keras.layers.Conv2D(8,(3,3),activation="relu",padding="same")(z8)
        
        
z10 = keras.layers.Conv2D(8,(3,3), activation="relu",padding="same")(z9)
z10 = keras.layers.Add()([z10,z9])
z11 = keras.layers.Conv2D(16,(3,3), activation="relu",padding="same")(z10)
z12 = keras.layers.UpSampling2D((2,2))(z11)
z13 = keras.layers.Conv2D(32,(3,3), activation="relu",padding="same")(z12)
z13 = keras.layers.Add()([z13,z6])
z14 = keras.layers.Conv2D(64,(3,3), activation="relu",padding="same")(z13)
z15 = keras.layers.UpSampling2D((2,2))(z14)
z16 = keras.layers.Conv2D(128,(3,3), activation="relu",padding="same")(z15)
z16 = keras.layers.Add()([z16,z3])
z17 = keras.layers.Conv2D(256,(3,3), activation="relu",padding="same")(z16)
        
z18 = keras.layers.Conv2D(1,(3,3), activation="linear",padding="same")(z17)

model=keras.models.Model(z1, z18)

model.compile(optimizer='Adam', loss='binary_crossentropy')

model.summary()

################################################################################
#
# Training network
#
################################################################################

model.fit(
    x_train_noisy, x_train,
    batch_size = 32,
    epochs = 12,
    shuffle=True,
    verbose = 2,
    validation_split=0.1
    )

################################################################################
#
# Saving network
#
################################################################################

model.save("shortcuts.h5")

