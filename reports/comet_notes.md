#Correction
##Task 1
###1)
2/2 P - Binary crossentropy is not well suited for this problem. Converging is very difficult with this loss. Use RMS! 
###2)
2/4 P - no convergion due to BCE
###3)
2/4 P - no convergion due to BCE
###4)
4/5 P - Results are not reasonable, since networks did not converge
##Task 2
5/5 P - again results are barly reasonable
#Total 15/20 P 



**RWTH Aachen - Deep Learning in Physics Research SS2020**

**Exercise 8 Denoising**

Date: 21.06.2020

Students:
| student            | rwth ID |
|--------------------|---------|
| Baptiste Corneglio | 411835|
| Florian Stadtmann  | 367319 |
| Johannes Wasmer    |  090800 |


# Task 1: Speckle removal#


In this exercise, all networks are trained with the Adam optimizer, with binary loss function and a batch size of 32.

##i)##


First, a shallow convolutional autoencoder is trained. Here the following structure is used:

    model=keras.models.Sequential(
        [
            keras.layers.Conv2D(16,(3,3), input_shape=(64,64,1), activation="relu",padding="same"),
            keras.layers.MaxPooling2D((2,2),padding="same"),
            keras.layers.Conv2D(8,(3,3),activation="relu",padding="same"),
            keras.layers.UpSampling2D((2,2)),
            keras.layers.Conv2D(1,(3,3), activation="linear",padding="same")
        ]
    )

The training took 163 epochs and was then ended due to the maximum allowed program length of 10 minutes. No significant overtraining is visible, but the improvement in the later epochs is not significant any more. Since the program was terminated, no model was saved. 
Therefore, the model was re-trained on 150 epochs to ensure that it does not stop early.
The final training loss is 0.4244 and the validation loss is 0.4276.


##ii)##
Second, a deeper network is used. Here we use the structure:

    model=keras.models.Sequential(
        [
            keras.layers.Conv2D(256,(3,3), input_shape=(64,64,1), activation="relu",padding="same"),
            keras.layers.Conv2D(128,(3,3), activation="relu",padding="same"),
            keras.layers.MaxPooling2D((2,2),padding="same"),
            keras.layers.Conv2D(64,(3,3), activation="relu",padding="same"),
            keras.layers.Conv2D(32,(3,3), activation="relu",padding="same"),
            keras.layers.MaxPooling2D((2,2),padding="same"),
            keras.layers.Conv2D(16,(3,3), activation="relu",padding="same"),
            keras.layers.Conv2D(8,(3,3),activation="relu",padding="same"),
            
    
            keras.layers.Conv2D(8,(3,3), activation="relu",padding="same"),
            keras.layers.Conv2D(16,(3,3), activation="relu",padding="same"),
            keras.layers.UpSampling2D((2,2)),
            keras.layers.Conv2D(32,(3,3), activation="relu",padding="same"),
            keras.layers.Conv2D(64,(3,3), activation="relu",padding="same"),
            keras.layers.UpSampling2D((2,2)),
            keras.layers.Conv2D(128,(3,3), activation="relu",padding="same"),
            keras.layers.Conv2D(256,(3,3), activation="relu",padding="same"),
            
            keras.layers.Conv2D(1,(3,3), activation="linear",padding="same")
        ]
    )

This time, training is stopped due to the 10 minutes cap at 13 epochs, so it is trained again for 12 epochs to get a saved model.
The final training loss is 0.4448 and the validation loss is 0.4397.
The validation loss ater the first epoch is lower than the validation loss of the shallower network, but already after the second epoch the shallow network beath the deep one. However, there is an improvement visible when training is terminated, so this structure might benefit from a longer training.

##iii)##

The third network uses a structure similar to the second one, but this time shortcuts are used.

    z1=keras.layers.Input(shape=(64,64,1))
    
    z2 = keras.layers.Conv2D(256,(3,3), activation="relu",padding="same")(z1)
    z3 = keras.layers.Conv2D(128,(3,3), activation="relu",padding="same")(z2)
    z4 = keras.layers.MaxPooling2D((2,2),padding="same")(z3)
    z5 = keras.layers.Conv2D(64,(3,3), activation="relu",padding="same")(z4)
    z6 = keras.layers.Conv2D(32,(3,3), activation="relu",padding="same")(z5)
    z7 = keras.layers.MaxPooling2D((2,2),padding="same")(z6)
    z8 = keras.layers.Conv2D(16,(3,3), activation="relu",padding="same")(z7)
    z9 = keras.layers.Conv2D(8,(3,3),activation="relu",padding="same")(z8)
            
            
    z10 = keras.layers.Conv2D(8,(3,3), activation="relu",padding="same")(z9)
    z10 = keras.layers.Add()([z10,z9])
    z11 = keras.layers.Conv2D(16,(3,3), activation="relu",padding="same")(z10)
    z12 = keras.layers.UpSampling2D((2,2))(z11)
    z13 = keras.layers.Conv2D(32,(3,3), activation="relu",padding="same")(z12)
    z13 = keras.layers.Add()([z13,z6])
    z14 = keras.layers.Conv2D(64,(3,3), activation="relu",padding="same")(z13)
    z15 = keras.layers.UpSampling2D((2,2))(z14)
    z16 = keras.layers.Conv2D(128,(3,3), activation="relu",padding="same")(z15)
    z16 = keras.layers.Add()([z16,z3])
    z17 = keras.layers.Conv2D(256,(3,3), activation="relu",padding="same")(z16)
            
    z18 = keras.layers.Conv2D(1,(3,3), activation="linear",padding="same")(z17)
    
    model=keras.models.Model(z1, z18)

Again, training is terminated after 10 minutes which correspondes to 13 epochs, so it is trained again for 12  epochs to get a saved model.
The training loss is 0.4126 and the validation loss is  0.4149. 
This network structure performs significantly better than the other two and again a longer training might lead to a lower loss.


##iv)##

The test losses are given in the table below:

| Network: | Shallow  | Deep | Shortcuts |
| Test Loss: | 0.4249 | 0.4371 | 0.4122 |

The input with noise "noisy" and the data without noise "real" is plotted together with the output of each of the networks.

![](https://www.comet.ml/api/image/notes/download?imageId=yBkxtA3aPXg9HBtwtcf5Zcn6T&objectId=da1fe110eb304b01a8aeb42c02d061a3)

One can see that the images of the shallow convolutional autoencoder removes some speckles, but a lot still remain.

The deep convolutional autoencoder removes speckles on larger scales, wich leads to a loss of smaller features in more detailed images.
It is well suited for less detailed images, but overall the loss is higher than for the shallow autoencoder. 

The deep convolutional autoencoder with shortcuts keeps the details while still removing a lot of the speckles. This yields the lowest loss.


#Task 2: Application to experimental data from partially coherent illumination#

Applying the network to the data yields the following plots:

![](https://www.comet.ml/api/image/notes/download?imageId=fO7XBUTVxD7BDFAAa8L2IExyN&objectId=da1fe110eb304b01a8aeb42c02d061a3)

One can see the same phenomena as mentioned before. The shallow convolutional autoencoder removes some speckles, the deep convolutional autoencoder gives a blurred image where all details are averaged and the deep convolutional autoencoder with shortcut removes more speckles than the shallow convolutional autoencoder in the high illuminated areas wihle keeping some detail in the low illuminated areas.



