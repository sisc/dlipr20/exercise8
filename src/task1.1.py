﻿from comet_ml import Experiment
import numpy as np
import dlipr
import speckles
from tensorflow import keras
import matplotlib.pyplot as plt
models = keras.models
layers = keras.layers
backend = keras.backend


################################################################################
#
# Connecting to Comet
#
################################################################################


experiment = Experiment(api_key="E7YQLfWBuQZXjw0c2dKsXKPsY",
                        project_name="exercise8", workspace="irratzo")



################################################################################
#
# Preprocessing data
#
################################################################################



def preprocess_data(data, norm_data):
    # logarithmic intensity values
    data = np.log10(data + 0.01)
    norm_data = np.log10(norm_data + 0.01)
    # norm input data to max. value of undistorted scattering pattern
    max_val = np.max(norm_data, axis=1)
    data = data / max_val.reshape(len(max_val), 1)
    # reshape data for convolutional network
    data = np.reshape(data, (len(data), 64, 64, 1))
    # limit maximum intensity values
    data = np.clip(data, 0., 1.1)

    return data


(x_train_noisy, x_train), (x_test_noisy, x_test) = speckles.load_data()

x_train_noisy = preprocess_data(x_train_noisy, x_train)
x_train = preprocess_data(x_train, x_train)
x_test_noisy = preprocess_data(x_test_noisy, x_test)
x_test = preprocess_data(x_test, x_test)

speckles.plot_examples(x_test_noisy, x_test, fname='speckle_examples.png')
print(x_train_noisy.shape)




################################################################################
#
# Defining network
#
################################################################################



model=keras.models.Sequential(
    [
        keras.layers.Conv2D(16,(3,3), input_shape=(64,64,1), activation="relu",padding="same"),
        keras.layers.MaxPooling2D((2,2),padding="same"),
        keras.layers.Conv2D(8,(3,3),activation="relu",padding="same"),
        keras.layers.UpSampling2D((2,2)),
        keras.layers.Conv2D(1,(3,3), activation="linear",padding="same")
    ]
)

model.compile(optimizer='Adam', loss='binary_crossentropy')

model.summary()

################################################################################
#
# Training network
#
################################################################################

model.fit(
    x_train_noisy, x_train,
    batch_size = 32,
    epochs = 150,
    shuffle=True,
    verbose = 2,
    validation_split=0.1
    )

#    callbacks = [keras.callbacks.EarlyStopping(
#        monitor='val_loss', patience=5
#        )]




################################################################################
#
# Saving network
#
################################################################################

model.save("shallow.h5")

