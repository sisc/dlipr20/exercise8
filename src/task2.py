﻿from comet_ml import Experiment
import numpy as np
import dlipr
import speckles
from tensorflow import keras
import matplotlib.pyplot as plt
models = keras.models
layers = keras.layers
backend = keras.backend


################################################################################
#
# Connecting to Comet
#
################################################################################


experiment = Experiment(api_key="E7YQLfWBuQZXjw0c2dKsXKPsY",
                        project_name="exercise8", workspace="irratzo")



################################################################################
#
# Preprocessing data
#
################################################################################


def preprocess_data(data, norm_data):
    # logarithmic intensity values
    data = np.log10(data + 0.01)
    norm_data = np.log10(norm_data + 0.01)
    # norm input data to max. value of undistorted scattering pattern
    max_val = np.max(norm_data, axis=1)
    data = data / max_val.reshape(len(max_val), 1)
    # reshape data for convolutional network
    data = np.reshape(data, (len(data), 64, 64, 1))
    # limit maximum intensity values
    data = np.clip(data, 0., 1.1)
    return data


# read in measured scattering patterns
real_names = ['cb019_100.npy', 'cb019_103.npy', 'AuCd_302_0K_H_III_1.npy']
real_imgs = np.empty((len(real_names), 64 * 64))

for i in range(len(real_names)):
    img = np.load(real_names[i])
    img = img[np.newaxis]
    real_imgs[i, :] = img

real_imgs = preprocess_data(real_imgs, real_imgs)


################################################################################
#
# Loading models
#
################################################################################


shallow = keras.models.load_model("shallow.h5")
deep = keras.models.load_model("deep.h5")
shortcuts = keras.models.load_model("shortcuts.h5")


################################################################################
#
# Calculating predictions
#
################################################################################

x=[]

x_name=["noisy","shallow","deep","shortcuts"]

x.append(real_imgs)
x.append(shallow.predict(real_imgs))
x.append(deep.predict(real_imgs))
x.append(shortcuts.predict(real_imgs))


################################################################################
#
# Plot Results
#
################################################################################


def plot_prediction_examples(x, xname, fname='prediction_task2.png'):
    # amount of different versions of same image
    p=len(x)
    # some indices of interesting test data
    n=x[0].shape[0]
    fig = plt.figure(figsize=(p,n))
    for j in range(p):
        for i in range(n):
            ax = plt.subplot(n, p, p*i+j+1)
            if i==0:
                ax.title.set_text(xname[j])
            plt.imshow(x[j][i,:,:,0])
            plt.xticks([])
            plt.yticks([])

    fig.savefig(fname)


plot_prediction_examples(x,x_name)





