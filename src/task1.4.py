﻿from comet_ml import Experiment
import numpy as np
import dlipr
import speckles
from tensorflow import keras
import matplotlib.pyplot as plt
models = keras.models
layers = keras.layers
backend = keras.backend


################################################################################
#
# Connecting to Comet
#
################################################################################


experiment = Experiment(api_key="E7YQLfWBuQZXjw0c2dKsXKPsY",
                        project_name="exercise8", workspace="irratzo")


################################################################################
#
# Preprocessing data
#
################################################################################

def preprocess_data(data, norm_data):
    # logarithmic intensity values
    data = np.log10(data + 0.01)
    norm_data = np.log10(norm_data + 0.01)
    # norm input data to max. value of undistorted scattering pattern
    max_val = np.max(norm_data, axis=1)
    data = data / max_val.reshape(len(max_val), 1)
    # reshape data for convolutional network
    data = np.reshape(data, (len(data), 64, 64, 1))
    # limit maximum intensity values
    data = np.clip(data, 0., 1.1)

    return data


(x_train_noisy, x_train), (x_test_noisy, x_test) = speckles.load_data()

x_train_noisy = preprocess_data(x_train_noisy, x_train)
x_train = preprocess_data(x_train, x_train)
x_test_noisy = preprocess_data(x_test_noisy, x_test)
x_test = preprocess_data(x_test, x_test)

speckles.plot_examples(x_test_noisy, x_test, fname='speckle_examples.png')
print(x_train_noisy.shape)



################################################################################
#
# Loading models
#
################################################################################


shallow = keras.models.load_model("shallow.h5")
deep = keras.models.load_model("deep.h5")
shortcuts = keras.models.load_model("shortcuts.h5")

################################################################################
#
# Evaluating test loss
#
################################################################################


print("shallow",shallow.evaluate(x_test_noisy, x_test, verbose=0))
print("deep",deep.evaluate(x_test_noisy, x_test,verbose=0))
print("shortcuts",shortcuts.evaluate(x_test_noisy, x_test,verbose=0))


################################################################################
#
# Calculating predictions
#
################################################################################

x=[]

x_name=["noisy","shallow","deep","shortcuts","real"]

x.append(x_test_noisy)
x.append(shallow.predict(x_test_noisy))
x.append(deep.predict(x_test_noisy))
x.append(shortcuts.predict(x_test_noisy))
x.append(x_test)


################################################################################
#
# Plot Results
#
################################################################################


def plot_prediction_examples(x, xname, fname='prediction_examples.png'):
    # amount of different versions of same image
    p=len(x)
    # some indices of interesting test data
    m = [1, 2, 4, 6, 10, 12, 14, 16, 22, 25, 27, 28, 30, 32, 35, 37, 39, 40, 41, 48, 49, 50, 52, 57, 61, 63, 64, 67, 70, 76]
    n=len(m)
    fig = plt.figure(figsize=(p,n))
    for j in range(p):
        for i in range(n):
            ax = plt.subplot(n, p, p*i+j+1)
            if i==0:
                ax.title.set_text(xname[j])
            plt.imshow(x[j][m[i],:,:,0])
            plt.xticks([])
            plt.yticks([])

    fig.savefig(fname)


plot_prediction_examples(x,x_name)

